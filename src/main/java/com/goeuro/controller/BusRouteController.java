package com.goeuro.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.dto.BusRoute;
import com.goeuro.service.BusRouteService;

@RestController
public class BusRouteController {

	@Autowired
	private BusRouteService busRouteService;
	
	@RequestMapping(value="/api/direct", method=GET)
	public BusRoute findDirectConnection(@RequestParam int dep_sid, @RequestParam int arr_sid){
		return busRouteService.findBusRoute(dep_sid, arr_sid);
	}
}
