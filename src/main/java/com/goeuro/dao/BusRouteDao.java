package com.goeuro.dao;

public interface BusRouteDao {

	void initializeBusRoutes(String fileName);
	
	boolean existsDirectRoute(int dep_sid, int arr_sid);
}
