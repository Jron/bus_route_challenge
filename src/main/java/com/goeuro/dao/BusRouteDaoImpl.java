package com.goeuro.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class BusRouteDaoImpl implements BusRouteDao {

	private List<List<Integer>> busRoutes = new ArrayList<>();
	
	@Override
	public void initializeBusRoutes(String fileName) {
		busRoutes.clear();
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	            String[] lineArray = line.split(" ");
	        	if (lineArray.length >= 3) {
	        		Integer[] lineIntegers = Arrays.stream(lineArray).map(Integer::parseInt).toArray(Integer[]::new);
	        		busRoutes.add(Arrays.asList(Arrays.copyOfRange(lineIntegers, 1, lineIntegers.length)));
	        	}
	        }
	    } catch (IOException e) {
	        System.out.println(e.getMessage());
	    }
	}

	@Override
	public boolean existsDirectRoute(int dep_sid, int arr_sid) {
		List<Integer> stations = Arrays.asList(dep_sid, arr_sid);
		for (List<Integer> currentRoutes: busRoutes) {
			if (currentRoutes.containsAll(stations)) {
				return true;
			}
		}
		return false;
	}

}
