package com.goeuro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

import com.goeuro.dao.BusRouteDao;

@SpringBootApplication
public class BusRouteChallengeApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(BusRouteChallengeApplication.class);
	    springApplication.addListeners(new ApplicationPidFileWriter("BusRouteChallengeApplication.pid"));
	    ApplicationContext ctx = springApplication.run(args);

	    BusRouteDao busRouteDao = ctx.getBean(BusRouteDao.class);
        busRouteDao.initializeBusRoutes(args[0]);
	}
}
