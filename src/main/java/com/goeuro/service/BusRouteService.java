package com.goeuro.service;

import com.goeuro.dto.BusRoute;

public interface BusRouteService {

	BusRoute findBusRoute(int dep, int arr);
}
