package com.goeuro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goeuro.dao.BusRouteDao;
import com.goeuro.dto.BusRoute;

@Service
public class BusRouteServiceImpl implements BusRouteService{

	@Autowired
	private BusRouteDao busRouteDao;
	
	@Override
	public BusRoute findBusRoute(int dep, int arr) {
		BusRoute route = new BusRoute();
		route.setDep_sid(dep);
		route.setArr_sid(arr);
		route.setDirect_bus_route( busRouteDao.existsDirectRoute(dep, arr));
		return route;
	}

	
}
