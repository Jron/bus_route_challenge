package com.goeuro.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.goeuro.configuration.AppTestContext;
import com.goeuro.dto.BusRoute;
import com.goeuro.service.BusRouteService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppTestContext.class})
@WebAppConfiguration
@ActiveProfiles("test")
public class BusRouteControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private BusRouteService busRouteServiceMock;
	
	@Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
	@Test
	public void testFindDirectConnection_valid() throws Exception {
		// Prepare
		int dep_sid = 1;
		int arr_sid = 2;
		BusRoute route = new BusRoute();
		route.setDep_sid(dep_sid);
		route.setArr_sid(arr_sid);
		route.setDirect_bus_route(true);
		Mockito.when(busRouteServiceMock.findBusRoute(dep_sid, arr_sid)).thenReturn(route);
		
		
		mockMvc.perform(MockMvcRequestBuilders.get("/api/direct?dep_sid={dep_sid}&arr_sid={arr_sid}", dep_sid, arr_sid))
	        .andExpect(status().isOk()).andDo(print())
	        .andExpect(jsonPath("$.dep_sid", is(equalTo(dep_sid))))
	        .andExpect(jsonPath("$.arr_sid", is(equalTo(arr_sid))))
	        .andExpect(jsonPath("$.direct_bus_route", is(equalTo(true))));
	}
}
