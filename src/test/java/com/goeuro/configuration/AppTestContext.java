package com.goeuro.configuration;

import static org.mockito.Mockito.mock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.goeuro.service.BusRouteService;

@Configuration

@EnableWebMvc
@ComponentScan(basePackages = {"com.goeuro.controller"})
@Profile("test")
public class AppTestContext extends WebMvcConfigurerAdapter {

    @Bean
    public BusRouteService searchService() {
        return mock(BusRouteService.class);
    }

}
