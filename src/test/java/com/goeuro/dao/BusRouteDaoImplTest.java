package com.goeuro.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;

public class BusRouteDaoImplTest {

	private BusRouteDao testable = new BusRouteDaoImpl();
	
	@Test
	public void testExistsDirectRoute_withoutDataFile() {
		assertFalse(testable.existsDirectRoute(0, 1));
	}
	
	@Test
	public void testExistsDirectRoute_withDataFile() {
		// Prepare
		URL url = Thread.currentThread().getContextClassLoader().getResource("testRoutes.data");
		testable.initializeBusRoutes(url.getPath());
		
		// Verify
		assertTrue(testable.existsDirectRoute(0, 1));
		assertTrue(testable.existsDirectRoute(6, 1));
		assertFalse(testable.existsDirectRoute(0, 5));
	}
}
