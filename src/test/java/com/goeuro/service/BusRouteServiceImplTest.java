package com.goeuro.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.goeuro.dao.BusRouteDao;
import com.goeuro.dto.BusRoute;

@RunWith(MockitoJUnitRunner.class)
public class BusRouteServiceImplTest {

	@InjectMocks
	private BusRouteService testable = new BusRouteServiceImpl();
	
	@Mock
	private BusRouteDao busRouteDaoMock;
	
	
	@Test
	public void testFindBusRoute_noDirectConnection() {
		// Prepare
		int dep_sid = -1;
		int arr_sid = -1;
		when(busRouteDaoMock.existsDirectRoute(dep_sid, arr_sid)).thenReturn(false);
		
		// Execute
		BusRoute busRoute = testable.findBusRoute(dep_sid, arr_sid);
		
		// Verify
		assertThat(busRoute.getDep_sid(), is(equalTo(dep_sid)));
		assertThat(busRoute.getArr_sid(), is(equalTo(arr_sid)));
		assertFalse(busRoute.isDirect_bus_route());
	}
	
	@Test
	public void testFindBusRoute_findDirectConnection() {
		// Prepare
		int dep_sid = 0;
		int arr_sid = 1;
		when(busRouteDaoMock.existsDirectRoute(dep_sid, arr_sid)).thenReturn(true);
		
		// Execute
		BusRoute busRoute = testable.findBusRoute(dep_sid, arr_sid);
		
		// Verify
		assertThat(busRoute.getDep_sid(), is(equalTo(dep_sid)));
		assertThat(busRoute.getArr_sid(), is(equalTo(arr_sid)));
		assertTrue(busRoute.isDirect_bus_route());
	}
}
